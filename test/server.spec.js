const expect = require('chai').expect;
const request = require('request');
const app = require('../src/server');
const port = 3000;

describe("Color code converter API", () => {
    let server = undefined;
    before("Start server", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening on localhost:${3000}`)
            done();
        })
    })
    describe("RGB top Hex conversion", () => {
        const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;
        it("returns status code 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200); //toimiva statuskoodi on 200
                done();
            })
        })
        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("ffffff");
                done();
            })

        })
    })
      describe("Hex to RGB conversion", function () {
          var url = "http://localhost:3000/hexToRgb?hex=00ff00";

          it("returns status 200", function (done) {
              request(url, function (error, response, body) {
                  expect(response.statusCode).to.equal(200);
                  done();
              });
          });

          it("returns the color in RGB", function (done) {
              request(url, function (error, response, body) {
                  expect(body).to.equal("[0,255,0]");
                  done();
              });
          });
      });



    after("Close Server", (done) => {
        server.close();
        done();

    })
})