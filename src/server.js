const express = require('express');
const req = require('express/lib/request');
const res = require('express/lib/response');
const app = express();
const converter = require('./converter');
const port = 3000;

// endpoint localhost:3000/
app.get('/',  (req, res) => res.send("Hello"))
// endpoint localhost:3000/rgb-to-hex?red=255&green=0&blue=0
app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.red, 10) //kymmenjärjestelmään (radix)
    const green = parseInt(req.query.green, 10)
    const blue = parseInt(req.query.blue, 10)
    const hex = converter.rgbToHex(red, green, blue)
    res.send(hex)
})
//endpoint localhost:3000/hexToRgb?hex=ffffff
app.get('/hexToRgb', (req, res) => {

    var hex = req.query.hex;
    var rgb = converter.hexToRgb(hex);
    res.send(JSON.stringify(rgb));

})


if (process.env.NODE_ENV === 'test') {
    module.exports = app;

} else {
    app.listen(port, () => console.log(`Server listening on localhost:${port}`)) //takahipsujen takia voi pistää muuttujan
}
