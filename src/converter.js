const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex ) //jos pituus on 1, palautetaan eka vaihtoehto, jos false, niin toka vaihtis.
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); //muuttujassa arvo kuten ff
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex); // "ff0000" pitää olla 6 merkkiä
    } // kotitehtävä: 
    ,
    hexToRgb: (hex) => {

    var red = parseInt(hex.substring(0, 2), 16);
    var green = parseInt(hex.substring(2, 4), 16);
    var blue = parseInt(hex.substring(4, 6), 16);

    return [red, green, blue];

}
}